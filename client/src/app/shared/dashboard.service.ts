import {Injectable} from "@angular/core";
import {HttpClient} from "@angular/common/http";
import {Observable, tap} from "rxjs";

export interface Board {
  id: number
  title: string
  completed: boolean
  name?: string
  date?: any
}

@Injectable({providedIn: 'root'})
export class DashboardService {
  public boards: Board[] = []

  constructor(public http: HttpClient) {
  }

  fetchBoards(): Observable<Board[]> {
    return this.http.get<Board[]>('https://jsonplaceholder.typicode.com/todos')
      .pipe(tap(boards => this.boards = boards))
  }

  /*public boards: Board[] = [
    {id:1, name: 'one', completed: false, date: new Date()},
    {id:2, name: 'two', completed: false, date: new Date()},
    {id:3, name: 'three', completed: false, date: new Date()},
    {id:4, name: 'four', completed: false, date: new Date()}
  ]*/

  removeBoard(id: number) {
    this.boards = this.boards.filter(board => board.id !== id)
  }

}
