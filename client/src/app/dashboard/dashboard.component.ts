import {Component, OnInit} from '@angular/core';
import {DashboardService} from "../shared/dashboard.service";
import {delay} from "rxjs";


@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

  public loading: boolean = true

  constructor(public dashboardService: DashboardService) { }

  ngOnInit(): void {
    this.dashboardService.fetchBoards()
      .pipe(delay(2000))
      .subscribe(()=>{
      this.loading = false
    })
  }

  removeBoard(id: number){
    this.dashboardService.removeBoard(id)
  }

}
